package sqlmarker.core;

/**
 * Convierte un nombre de columna en donde las palabras estan separadas por '_' en CamelCase.
 * Ej. emp_first_name  => empFirstName.
 */
public class CamelNameMappingStrategy implements NameMappingStrategy {

   public String mapColumn(String column) {
        if (column == null) return "";
        String[] part = column.split("_");
        if (part.length <= 1) return column.toLowerCase();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < part.length; i++) {
            String word = part[i];
            if (i == 0){
                word = word.toLowerCase();
            }else{
                word = word.substring(0,1).toUpperCase() + word.substring(1).toLowerCase();
            }
            result.append(word);
        }
        return result.toString();
    }

}
