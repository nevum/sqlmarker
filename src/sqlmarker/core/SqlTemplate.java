package sqlmarker.core;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.KeyHolder;
import sqlmarker.cache.XmlTemplateLoader;
import sqlmarker.translator.OracleTranslator;
import sqlmarker.translator.SqlServerTranslator;

/**
 * Template class to handle basic sql operations
 * @author Cesar
 */
@Slf4j
public class SqlTemplate {
    
    private static final String DEBUG_QUERY_MSG = "Query {} <<{}>> params {}";
    private static final String DEBUG_UPDATE_MSG = "Update {} <<{}>> params {}";
    
    private final NamedParameterJdbcTemplate template;

    private final SqlMarker sqlMarker;
        
    private SqlTranslator translator;
    
    @Autowired
    public SqlTemplate(DataSource dataSource, SqlMarker sqlMarker) throws SQLException {
        this.template = new NamedParameterJdbcTemplate(dataSource);
        this.sqlMarker = sqlMarker;
    }
    
    @PostConstruct
    public void init() throws Exception {
        switch (sqlMarker.getDbms()) {
            case "mss":
                translator = new SqlServerTranslator();
                break;
            case "ora":
                translator = new OracleTranslator();
                break;
            default :
                log.warn("No valid sql.dbms specified or is empty, defaulting to 'ora'");
                translator = new OracleTranslator();
        }        
    }
    
    public void setMaxRows(int maxRows) {
        template.getJdbcTemplate().setMaxRows(maxRows);
    }
    
    public int getMaxRows() {
        return template.getJdbcTemplate().getMaxRows();
    }
    
    public <T> List<T> query(String templateName, SqlParams params, RowMapper<T> rowMapper) {
        final SqlParams allParams = getAllParams(params);
        final Map<String, ?> paramsMap = allParams.toMap();
        final String sql = translate(sqlMarker.process(templateName, paramsMap));
        if (debugEnabled(templateName)) {
            log.debug(DEBUG_QUERY_MSG, new Object[] {templateName, sql, paramsMap});
        }
        return template.query(sql, allParams.toSqlParameterSource(), rowMapper);
    }

    public <T> List<T> query(String templateName, RowMapper<T> rowMapper) {
        return query(templateName, null, rowMapper);
    }
    
    public <T> List<T> query(String templateName, Class<T> requiredType) {
        return query(templateName, null, new SmartRowMapper<>(requiredType));
    }
    
    public <T> List<T> query(String templateName, SqlParams params, Class<T> requiredType) {
        return query(templateName, params, new SmartRowMapper<>(requiredType));
    }
    
    public <T> T queryForObject(String templateName, SqlParams params, RowMapper<T> rowMapper) {
        final SqlParams allParams = getAllParams(params);
        final Map<String, ?> paramsMap = allParams.toMap();
        final String sql = translate(sqlMarker.process(templateName, paramsMap));
        if (debugEnabled(templateName)) {
            log.debug(DEBUG_QUERY_MSG, new Object[] {templateName, sql, paramsMap});
        }
        return template.queryForObject(sql, allParams.toSqlParameterSource(), rowMapper);
    }
    
    public <T> T queryForObject(String templateName, SqlParams params, Class<T> requiredType) {
        return queryForObject(templateName, params, new SmartRowMapper<>(requiredType));
    }

    public <T> T queryForScalar(String templateName, SqlParams params, Class<T> requiredType) {
        final SqlParams allParams = getAllParams(params);
        final Map<String, ?> paramsMap = allParams.toMap();
        final String sql = translate(sqlMarker.process(templateName, paramsMap));
        if (debugEnabled(templateName)) {
            log.debug(DEBUG_QUERY_MSG, new Object[] {templateName, sql, paramsMap});
        }
        return template.queryForObject(sql, allParams.toSqlParameterSource(), requiredType);
    }

    public <T> List<T> queryForList(String templateName, SqlParams params, Class<T> requiredType) {
        final SqlParams allParams = getAllParams(params);
        final Map<String, ?> paramsMap = allParams.toMap();
        final String sql = translate(sqlMarker.process(templateName, paramsMap));
        if (debugEnabled(templateName)) {
            log.debug(DEBUG_QUERY_MSG, new Object[] {templateName, sql, paramsMap});
        }
        return template.queryForList(sql, allParams.toSqlParameterSource(), requiredType);
    }
    
    public int update(String templateName, SqlParams params) {
        final SqlParams allParams = getAllParams(params);
        final Map<String, ?> paramsMap = allParams.toMap();
        final String sql = translate(sqlMarker.process(templateName, paramsMap));
        if (debugEnabled(templateName)) {
            log.debug(DEBUG_UPDATE_MSG, new Object[] {templateName, sql, paramsMap});
        }
        return template.update(sql, allParams.toSqlParameterSource());
    }
    
    public int update(String templateName, SqlParams params, KeyHolder kh, String[] keyColumnsNames) {
        final SqlParams allParams = getAllParams(params);
        final Map<String, ?> paramsMap = allParams.toMap();
        final String sql = translate(sqlMarker.process(templateName, paramsMap));
        if (debugEnabled(templateName)) {
            log.debug(DEBUG_UPDATE_MSG, new Object[] {templateName, sql, paramsMap});
        }
        return template.update(sql, allParams.toSqlParameterSource(), kh, keyColumnsNames);        
    }
    
    public JdbcTemplate getJdbcTemplate() {
        return template.getJdbcTemplate();
    }

    private String translate(String sql) {
        return translator != null? translator.translate(sql) : sql;
    }

    private SqlParams getAllParams(SqlParams params) {
        SqlParams allParams = params != null ? params.addMap(sqlMarker.computeContextMap()) : SqlParams.params(sqlMarker.computeContextMap());
        return allParams;
    }
      
    private static boolean debugEnabled(String templateName) {
        return log.isDebugEnabled() && !"none".equals(XmlTemplateLoader.getLogLevel(templateName));
    }
    
}
