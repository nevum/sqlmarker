package sqlmarker.translator;

import sqlmarker.core.SqlTranslator;

/**
 * Translate templates to sql server dialect
 * @author Cesar
 */
public class SqlServerTranslator implements SqlTranslator {

    @Override
    public String translate(String sql) {
        return sql.replace("||", "+");
    }
    
}
