package sqlmarker.translator;

import sqlmarker.core.SqlTranslator;

/**
 * Translate templates to Oracle dialect
 * @author Cesar
 */
public class OracleTranslator implements SqlTranslator {

    @Override
    public String translate(String sql) {
        return sql;
    }
    
}
