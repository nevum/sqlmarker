package sqlmarker.core;

/**
 * Estrategia de conversion de nombres entre beans y campos de tablas
 */
interface NameMappingStrategy {

    /**
     * Convierte una nombre de columna a un nombre de propiedad de Bean.
     * @param column nombre de la columna.
     * @return nombre del la propiedad del bean.
     */
    public String mapColumn(String column);

}
