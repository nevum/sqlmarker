package sqlmarker.core;

import java.util.HashMap;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.lang.NonNull;

/**
 * Sql Parameters
 * @author Cesar
 */
public class SqlParams {
    
    private Map<String,Object> params = new HashMap<>();
    
    private SqlParameterSource composedParams;
    
    public static SqlParams params() {
        return new SqlParams();
    }

    public static SqlParams params(@NonNull String name, @NonNull Object value) {
        return params().add(name, value);
    }
         
    public static SqlParams params(@NonNull Object obj) {
        return params().addObject(obj);
    }
    
    public static SqlParams params(@NonNull Map<String,?> values) {
        return params().addMap(values);
    }
    
    public SqlParams add(@NonNull String name, @NonNull Object value) {
        params.put(name, value);
        return this;
    }
    
    public SqlParams addObject(@NonNull Object obj) {
        BeanPropertySqlParameterSource bp = new BeanPropertySqlParameterSource(obj);
        composeParams(bp);
        return this;
    }

    public SqlParams addMap(@NonNull Map<String, ?> map) {
        MapSqlParameterSource mp = new MapSqlParameterSource(map);
        composeParams(mp);
        return this;
    }
    
    public void addSource(@NonNull SqlParameterSource source) {
        if (composedParams == null) {
            composedParams = source;
        } else {
            composedParams = new CompositeSqlParameterSource(composedParams, source);
        }
    }
    
    private SqlParameterSource packParams() {
        final SqlParameterSource packedParams = new MapSqlParameterSource(params);
        params = new HashMap<>();
        return packedParams;
    }
    
    private void composeParams(@NonNull SqlParameterSource source2) {
        if (params.isEmpty()) {
            addSource(source2);
        } else {
            addSource(packParams());
            addSource(source2);
        }
    }
        
    public SqlParameterSource toSqlParameterSource() {
        final MapSqlParameterSource mapSource = new MapSqlParameterSource(params);
        if (composedParams != null) {
            return new CompositeSqlParameterSource(composedParams, mapSource);
        }
        return mapSource;
    }
    
    public Map<String,?> toMap() {
        final Map<String,Object> result = new HashMap<>();
        final SqlParameterSource source = toSqlParameterSource();
        String[] names = source.getParameterNames();
        if (names == null) {
            return result;
        }
        for (String name : names) {
            result.put(name, source.getValue(name));
        }
        return result;
    }
    
}
