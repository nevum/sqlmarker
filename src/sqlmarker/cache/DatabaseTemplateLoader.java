package sqlmarker.cache;

import freemarker.cache.TemplateLoader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.dao.DataAccessException;

/**
 * Load Templates from database
 * @author Cesar
 */
public class DatabaseTemplateLoader implements TemplateLoader {
    
    private static final Logger logger = LoggerFactory.getLogger(DatabaseTemplateLoader.class);
    
    private final NamedParameterJdbcTemplate db;

    public DatabaseTemplateLoader(DataSource datasource) {
        this.db = new NamedParameterJdbcTemplate(datasource);
    }
    
    @Override
    public Object findTemplateSource(final String name) throws IOException {
        if (name.matches(".*_..$")) {
            return null;
        }
        Map<String, Object> params = new HashMap<>(1);
        params.put("name", name);
        try {
            String template = db.queryForObject("SELECT template FROM att_sql_templates WHERE name = :name", params, String.class);
            return template;
        } catch (DataAccessException ex) {
            logger.error("Error loading template '{}'", name, ex);
            throw new IOException("Error loading template '" + name + '\'', ex);
        } 
    }

    @Override
    public long getLastModified(final Object templateSource) {
        return -1;
    }

    @Override
    public Reader getReader(final Object templateSource, final String encoding) throws IOException {
        return new StringReader((String) templateSource);
    }

    @Override
    public void closeTemplateSource(Object templateSource) throws IOException {
        // Do nothing
    }
    
}
