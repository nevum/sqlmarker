/* CUIDADO!!!  DROPS SI ES NECESARIO  
--DROP TABLE att_sql_templates;
--DROP SEQUENCE att_sql_templates_seq;
--DROP CLUSTER att_sql_cluster;
*/

CREATE CLUSTER att_sql_cluster (sql_template_id integer);

CREATE INDEX att_sql_cluster_idx ON CLUSTER att_sql_cluster;

CREATE SEQUENCE att_sql_templates_seq START WITH 1 NOCACHE;
CREATE TABLE att_sql_templates (
    id INTEGER NOT NULL,
    name VARCHAR2(250) NOT NULL, 
    template VARCHAR2(4000 char),
    last_modified DATE default sysdate,
    aud_object_type VARCHAR2(2),
    aud_action VARCHAR2(2),
    audit_format VARCHAR2(250),
    CONSTRAINT att_sql_templates_pk PRIMARY KEY ( id )
) CLUSTER att_sql_cluster (id);

CREATE UNIQUE INDEX att_sql_templates_name_uk ON att_sql_templates (name);

CREATE SEQUENCE att_sql_templates_seq start with 1 nocache;

DROP TRIGGER bi_att_sql_templates;

CREATE OR REPLACE TRIGGER bi_att_sql_templates
BEFORE INSERT ON att_sql_templates 
FOR EACH ROW
WHEN (new.id IS NULL)
BEGIN
  :new.id := att_sql_templates_seq.NEXTVAL;
  :new.last_modified := sysdate;
END;
/

CREATE OR REPLACE TRIGGER bu_att_sql_templates
BEFORE UPDATE ON att_sql_templates 
FOR EACH ROW
BEGIN
  :new.last_modified := sysdate;
END;
/




