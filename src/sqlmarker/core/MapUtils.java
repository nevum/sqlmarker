package sqlmarker.core;

import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Utilities for Map handling
 * @author Cesar
 */
public class MapUtils {

    private MapUtils() { }
    
    public static Map.Entry<String, Object> entry(String name, Object value) {
        return new AbstractMap.SimpleImmutableEntry<>(name, value);
    }
    
    @SafeVarargs
    public static Map<String, Object> toMap(Map.Entry<String, Object> ...params) {
        return Stream.of(params).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
    
}
