CREATE TABLE att_sql_templates (
    id INTEGER NOT NULL IDENTITY,
    name VARCHAR(250) NOT NULL, 
    template VARCHAR(4000),
    last_modified DATETIME default getDate(),
    aud_object_type VARCHAR(2),
    aud_action VARCHAR(2),
    audit_format VARCHAR(250),
    CONSTRAINT att_sql_templates_pk PRIMARY KEY ( id )
);

CREATE UNIQUE INDEX att_sql_templates_name_uk ON att_sql_templates (name);

DROP TRIGGER att_sql_templates_u;
CREATE TRIGGER att_sql_templates_u 
  ON att_sql_templates 
  AFTER UPDATE
AS
BEGIN
  UPDATE att_sql_templates
    SET last_modified = getDate()
  FROM att_sql_templates s
   JOIN inserted i ON i.id = s.id
END;

/*
CREATE TABLE att_sql_template_roles (
  id INTEGER NOT NULL,
  role VARCHAR(20) NOT NULL,
  CONSTRAINT att_sql_template_roles_fk FOREIGN KEY ( id ) REFERENCES att_sql_templates( id )
);
*/
-- Utiles para migracion

-- Crea Backup
DROP TABLE att_sql_templates_bkp;
SELECT name,template, last_modified, aud_object_type,aud_action,audit_format
INTO att_sql_templates_bkp
FROM att_sql_templates;

-- Dropear todo
DROP TABLE att_sql_template_roles;
DROP TABLE att_sql_templates;

-- Reinsertar Templates
INSERT INTO att_sql_templates(name,template, last_modified, aud_object_type,aud_action,audit_format)
SELECT name,template, last_modified, aud_object_type,aud_action,audit_format
FROM att_sql_templates_bkp;
