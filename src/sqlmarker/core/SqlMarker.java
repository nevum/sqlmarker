package sqlmarker.core;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import java.io.IOException;
import java.io.StringWriter;
import java.util.AbstractMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpSession;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.context.request.RequestContextHolder;
import sqlmarker.cache.XmlTemplateLoader;
import sqlmarker.directives.SysdateDirective;

/**
 * Template sql statements using FreeMarker
 * @author Cesar
 */

public class SqlMarker {
    
    private static final Logger logger = LoggerFactory.getLogger(SqlMarker.class);
    
    private Configuration cfg;
    
    @Value("${sql.custom}")
    @Getter @Setter private String custom;
    
    @Value("${sql.dbms}")
    @Getter @Setter private String dbms;
    
    @Value(value = "${sql.prefix:/WEB-INF/sql/}")
    @Getter @Setter private String prefix;
    
    @Autowired
    private ResourceLoader resourceLoader;
    
    private Map<String,String> sessionParams;
    
    @Autowired
    Environment env;
    
    @Autowired 
    HttpSession session;
    
    @PostConstruct
    public void init() throws Exception {
        cfg = new Configuration(Configuration.VERSION_2_3_28);
        cfg.setTagSyntax(Configuration.SQUARE_BRACKET_TAG_SYNTAX);
        cfg.setTemplateLoader(new XmlTemplateLoader(resourceLoader, prefix,dbms, custom));
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        cfg.setLogTemplateExceptions(false);
        cfg.setWrapUncheckedExceptions(true);
        cfg.setSharedVariable( "dbms", dbms);
        cfg.setSharedVariable( "custom", custom);
        cfg.setSharedVariable( "sysdate", new SysdateDirective(dbms));
    }
    
    public String process(String templateName, Object model) {
        try {
            Template template = cfg.getTemplate(templateName);
            StringWriter sw = new StringWriter();
            template.process(model, sw);
            return sw.toString();
        } catch (IOException | TemplateException ex) {
            logger.error("Error processing template {} : {}", templateName, ex.getMessage());
            throw new IllegalArgumentException(ex.getMessage(), ex);
        }
    }

    public void setSessionParams(Map<String, String> sessionParams) {
        this.sessionParams = sessionParams;
    }
    
    public Map<String, Object> computeContextMap() {
        return new SessionMapAdapter(session, sessionParams);
    }
    
    public static class SessionMapAdapter extends AbstractMap<String, Object> {

        private final Set<Entry<String, Object>> entries;
        
        public SessionMapAdapter(HttpSession session, Map<String,String> sessionsParams) {
            entries = new HashSet<>();
            if (session == null || sessionsParams == null || RequestContextHolder.getRequestAttributes() == null ) {
                return;
            }
            for (Entry<String,String> param : sessionsParams.entrySet()) {
                Entry<String,Object> entry = new SimpleImmutableEntry<>(param.getKey(), session.getAttribute(param.getValue()));
                entries.add(entry);
            }
        }        
        
        @Override
        public Set<Entry<String, Object>> entrySet() {
            return entries;
        }
        
    }
        
}
