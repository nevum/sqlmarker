package sqlmarker.core;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.jdbc.core.RowMapper;

/**
 * RowMapper que mapea por convencion
 * alternativa de spring es org.springframework.jdbc.core.BeanPropertyRowMapper
 * @author Cesar
 */
public class SmartRowMapper<T> implements RowMapper<T>{
    
    private static final Logger logger = LoggerFactory.getLogger(SmartRowMapper.class);
     
    private final Class<? extends T> klass;

    private List<String> columnNames = null;
    
    public SmartRowMapper(Class<? extends T> klass) {
        this.klass = klass;
    }

    private List<String> getColumnNames(ResultSet rs, NameMappingStrategy nameMapping) throws SQLException {
        ResultSetMetaData meta = rs.getMetaData();
        int count = meta.getColumnCount();
        List<String> result = new ArrayList<>(count);
        for (int i = 1; i <= count; i++) {
            result.add(nameMapping.mapColumn(meta.getColumnLabel(i)));
        }
        return result;
    }    
    
    private void setProperty(Object obj, String propName, Object data) {
        Field field = null;
        Object value = null;
        try {
            BeanWrapper wrapper = PropertyAccessorFactory.forBeanPropertyAccess(obj);
            value = coerce(data, wrapper.getPropertyDescriptor(propName).getPropertyType());
            wrapper.setPropertyValue(propName, value);
        } catch (RuntimeException e){
            logSetPropertyError(e, field, propName, obj, data, value);
            throw e;
        } catch (Exception e) {
            logSetPropertyError(e, field, propName, obj, data, value);
            runtimeError(e);
        }
    }
 
    private Class<?> dbFieldClass(T obj, String propName) {
        Field field = null;
        try {
            field = klass.getDeclaredField(propName);
            Class<?> fieldClass = field.getType();
            if (fieldClass.equals(Integer.class)
                || fieldClass.equals(String.class)
                || fieldClass.equals(LocalDate.class)
                || fieldClass.equals(LocalTime.class)
                || fieldClass.equals(LocalDateTime.class)
                || fieldClass.equals(OffsetDateTime.class)
                || fieldClass.equals(BigDecimal.class)) {
                return fieldClass;
            }
            /*
            String ftype = field != null ? field.getType().getSimpleName() : "<unknown>";
            logger.error("Illegal type {}  for field '{}' of {}.", new Object[] { ftype, propName, obj });
            throw new IllegalArgumentException("illegal field type " + ftype);
            */
        } catch (RuntimeException e) {
            logGetFieldError(e, field, propName, obj);
            throw e;
        } catch (Exception e) {
            logGetFieldError(e, field, propName, obj);
            runtimeError(e);
        }
        return null;
    }
        
    
    private Object coerce(Object data, Class<?> type) throws SQLException {
        if (data == null) {
            if (type.equals(int.class) 
                || type.equals(long.class) 
                || type.equals(double.class)
                || type.equals(float.class)) {
                return 0;
            } else {
                return null;
            }
        }
        if (data instanceof java.sql.Date) {
            java.sql.Date sqlDate = (java.sql.Date) data;
            if (type.equals(Date.class)) {
                return new Date(sqlDate.getTime());
            }
            if (type.equals(LocalDate.class)) {
                return sqlDate.toLocalDate();
            }
            if (type.equals(LocalDateTime.class)) {
                return sqlDate.toLocalDate().atStartOfDay();
            }
            if (type.equals(Instant.class)) {
                return sqlDate.toInstant();
            }
        }
        if (data instanceof java.util.Date) {
            Date date = (java.util.Date) data;
            if (type.equals(LocalDate.class)) {
                return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            }
            if (type.equals(LocalDateTime.class)) {
                return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            }
            if (type.equals(LocalTime.class)) {
                return date.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();
            }
            if (type.equals(Instant.class)) {
                return date.toInstant();
            }
        }        
//        if (data instanceof oracle.sql.TIMESTAMP) {
//            oracle.sql.TIMESTAMP ts = (oracle.sql.TIMESTAMP) data;
//            return new Date(ts.timestampValue().getTime());
//        }
        //FIXME: aqui no deberia entrar porque un Timestamp es un java.util.Date
        if (data instanceof Timestamp) {            
             return new Date(((Timestamp)data).getTime());
        }
        if (data instanceof BigDecimal){
           if (type.equals(int.class) || type.equals(Integer.class)) {
               return  ((BigDecimal) data).intValue();
           }
           if (type.equals(double.class) || type.equals(Double.class)) {
               return ((BigDecimal) data).doubleValue();
           }
        }        
        return data;
    }

    @Override
    public T mapRow(ResultSet rs, int i) throws SQLException {
        if (columnNames == null) {
            columnNames = getColumnNames(rs, new CamelNameMappingStrategy());
        }
        try {
            T obj = klass.newInstance();
            for (int colIndex = 0; colIndex < columnNames.size(); colIndex++) {
                //Class<?> dbClass = dbFieldClass(obj, columnNames.get(colIndex));
                //if (dbClass == null) {
                    setProperty(obj, columnNames.get(colIndex), rs.getObject(colIndex+1));
                //} else  {
                //    setProperty(obj, columnNames.get(colIndex), rs.getObject(colIndex+1, dbClass));
                //}
                
            }
            return obj;
        } catch (InstantiationException | IllegalAccessException ex) {
            logger.error(ex.getMessage(), ex);
            throw new RuntimeException(ex.getMessage());
        }
    }
     
    private <T> void logSetPropertyError(Throwable e, Field field, String propName, T obj,
                                         Object data, Object value) {
        String ftype = field != null ? field.getType().getSimpleName() : "<unknown>";
        String vtype = value != null ? value.getClass().getSimpleName() : "null";
        logger.error("Error {} when setting field '{}'({}) of {} with value {} converted to {}({}).",
                new Object[]{ e.toString(), propName, ftype, obj, data, value, vtype });
    }

    private void logGetFieldError(Throwable e, Field field, String propName, T obj) {
        String ftype = field != null ? field.getType().getSimpleName() : "<unknown>";
        logger.error("Error {} when getting field '{}'({}) of {}.",
                new Object[]{ e.toString(), propName, ftype, obj });
    }
    
    private void runtimeError( Exception e) {
        logger.error(e.getMessage(), e);
        throw new RuntimeException(e.getMessage(),e);
    }    


}
