package sqlmarker.directives;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;

/**
 * Directiva que implementa sysdate de Oracle en diferentes Bases de Datos
 * @author Cesar
 */
public class SysdateDirective extends AbstractDirective {

    public SysdateDirective(String dbms) {
        super(dbms);
    }

    @Override
    public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {
        if (!params.isEmpty()) {
            throw new TemplateModelException("This directive doesn't allow parameters.");
        }
        if (loopVars.length != 0) {
                throw new TemplateModelException( "This directive doesn't allow loop variables.");
        }
        if (body != null) { 
            throw new TemplateModelException( "This directive doesn't allow body.");
        }
        Writer out = env.getOut();
        switch (dbms) {
            case "ora": 
                out.write(" sysdate ");
                break;
            case "mss":
                out.write(" getdate() ");
                break;
            default:
                out.write(" current_timestamp "); // devuelve la hora del cliente
        }
    }
    
}
