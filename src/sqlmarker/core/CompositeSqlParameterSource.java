package sqlmarker.core;

import java.util.Arrays;
import org.springframework.jdbc.core.namedparam.AbstractSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 * Allow to compose two sources of parameters
 * @author Cesar
 */
public class CompositeSqlParameterSource extends AbstractSqlParameterSource {

    private final SqlParameterSource source1;
    private final SqlParameterSource source2;

    public CompositeSqlParameterSource(SqlParameterSource source1, SqlParameterSource source2) {
        this.source1 = source1;
        this.source2 = source2;
    }
    
    @Override
    public boolean hasValue(String name) {
        return source1.hasValue(name) || source2.hasValue(name);
    }

    @Override
    public Object getValue(String name) throws IllegalArgumentException {
        try {
            return source1.getValue(name);
        } catch (IllegalArgumentException e) {
            return source2.getValue(name);
        }
    }

    @Override
    public String[] getParameterNames() {
        return concatenate(source1.getParameterNames(), source2.getParameterNames()); //To change body of generated methods, choose Tools | Templates.
    }
    
    // Generic function to merge arrays of same type in Java
    // https://www.techiedelight.com/merge-two-arrays-java/
    private <T> T[] concatenate(T[] first, T[] second)
    {
	T[] result = Arrays.copyOf(first, first.length + second.length);
	System.arraycopy(second, 0, result, first.length, second.length);
	return result;
    }
    
}
