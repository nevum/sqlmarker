package sqlmarker.cache;

import freemarker.cache.TemplateLoader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import nu.xom.Attribute;
import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Node;
import nu.xom.ParsingException;
import nu.xom.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ResourceLoader;

/**
 * Load Templates from xml files
 * @author Cesar
 */
public class XmlTemplateLoader implements TemplateLoader {

    private static final Logger logger = LoggerFactory.getLogger(XmlTemplateLoader.class);

    private ResourceLoader resourceLoader;
    private String prefix;
    private String dbms;
    private String custom;
    private static final Map<String,String> logLevels = new ConcurrentHashMap<>();
    
    public XmlTemplateLoader(ResourceLoader resourceLoader, String prefix, String dbms, String custom) throws IOException {
        this.resourceLoader = resourceLoader;
        this.prefix = prefix;
        this.dbms = dbms;
        this.custom = custom;
    }
    
    @Override
    public void closeTemplateSource(Object templateSource) {
        // Do Nothing
    }

    @Override
    public Reader getReader(final Object templateSource, final String encoding) throws IOException {
        return new StringReader(((XmlTemplateSource)templateSource).template);
    }
    
    @Override
    public long getLastModified(Object templateSource) {
        return ((XmlTemplateSource)templateSource).lastModified;
    }

    @Override
    public Object findTemplateSource(String name) throws IOException {
         if (name.matches(".*_..$")) {
            return null;
        }
        String[] parts = name.split("/");
        if (parts.length != 2) {
            throw new IllegalArgumentException("Illegal template name '" + name + "' (may be missing '/' )" );
        }
        final XmlTemplateSource source = new XmlTemplateSource();
        source.filename = parts[0] + ".xml";
        source.sqlId = parts[1];
        Document doc;  
        try {
            Builder parser = new Builder();                      
             doc = parser.build(resourceLoader.getResource(prefix + source.filename).getInputStream());
        } catch (ParsingException ex) {
            throw new IOException("Parsing error " + source.filename + ": " + ex.getMessage());
        }
        source.lastModified = -1;
        source.template = findSql(doc, source, name);        
        return source;
    }

    private String findSql(Document doc, XmlTemplateSource source, String name) throws IOException {
        Element templates =  doc.getRootElement();
        if (!templates.getLocalName().equals("templates")) {
            throw new IOException("templates element missing");
        }
        Element selected = null;
        int selectedScore = 0;
        for (Element sql : templates.getChildElements("sql")) {
            if (sql.getAttribute("id") != null && source.sqlId.equals(sql.getAttribute("id").getValue())) {
                int sqlScore = 1; // inicializa el score en 1
                
                // Agrega score por dbms si fue especificada
                final Attribute xmlDbms = sql.getAttribute("dbms");                
                if (dbms != null 
                        && xmlDbms != null
                        && dbms.equals(xmlDbms.getValue())) {
                    sqlScore += 2;
                }
                
                // Agrega score por custom si fue especificado                
                final Attribute xmlCustom = sql.getAttribute("custom");                
                if (custom != null
                        && xmlCustom != null 
                        && custom.equals(xmlCustom.getValue())) {                    
                    sqlScore += 4;
                }

                // Compara con el seleccionado
                if (selectedScore < sqlScore) {
                    selected = sql;
                    selectedScore = sqlScore;
                }                
                
            };
        }
        if (selected == null) {
            logger.error("Not found template id '{}' on file {}", source.sqlId, source.filename);
            throw new IOException("Not found template id '" + source.sqlId + "' on file " + source.filename);
        }
        if (selected.getChildCount() == 0 ) {
            logger.error("Not sql body for id '{}' on file {}", source.sqlId, source.filename);
            throw new IOException("Not sql body for id '" + source.sqlId + "' on file " + source.filename);
        }
        final Node node = selected.getChild(0);
        if (!(node instanceof Text)) {
            logger.error("Not sql text for id '{}' on file {}", source.sqlId, source.filename);
            throw new IOException("Not sql text for id '" + source.sqlId + "' on file " + source.filename);            
        }
        final Attribute logLevelAttr = selected.getAttribute("log");                
        if (logLevelAttr != null && logLevelAttr.getValue() != null) {
            logLevels.put(name, logLevelAttr.getValue());
        }
        return ((Text) node).getValue();
    }
       
    public static String getLogLevel(String name) {
        return logLevels.get(name);
    }
    
    private class XmlTemplateSource {
        private String filename;
        private String sqlId;
        private String template;
        private long lastModified;        
    }
    
}
