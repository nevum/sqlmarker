package sqlmarker.core;

/**
 * Translates query templates
 * @author Cesar
 */
public interface SqlTranslator {
    
    String translate(String sql);
    
}
