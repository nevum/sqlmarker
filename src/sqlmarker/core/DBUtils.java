package sqlmarker.core;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 * Database Utilities
 *
 * @author Cesar
 */
public class DBUtils {

    public static String getProvider(DataSource dataSource) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            DatabaseMetaData meta = conn.getMetaData();
            String productName = meta.getDatabaseProductName();
            String provider = null;
            if (productName.equals("Microsoft SQL Server")) {
                provider = "mss";
            }
            if (productName.equals("Oracle")) {
                provider = "ora";
            }
            return provider;
        }
    }

}
