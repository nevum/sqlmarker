package sqlmarker.directives;

import freemarker.template.TemplateDirectiveModel;

/**
 * Clase base de las directivas de sqlmarker
 * @author Cesar
 */
public abstract class AbstractDirective implements TemplateDirectiveModel {

    protected final String dbms;

    public AbstractDirective(String dbms) {
        this.dbms = dbms;
    }

}
